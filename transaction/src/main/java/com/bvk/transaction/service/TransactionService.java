package com.bvk.transaction.service;

import com.bvk.apienv.exception.ResourceNotFoundException;
import com.bvk.apienv.model.ResponseApi;
import com.bvk.transaction.model.db.Transaction;
import com.bvk.transaction.model.db.dto.TransactionDto;
import com.bvk.transaction.model.db.param.TransactionSaveBodyParam;
import com.bvk.transaction.model.db.param.TransactionUpdateBodyParam;
import com.bvk.transaction.model.db.value.Status;
import com.bvk.transaction.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    public ResponseApi save(TransactionSaveBodyParam transactionSaveBodyParam) {
        Transaction transaction = transactionRepository
            .save(
                Transaction.builder()
                    .userId(transactionSaveBodyParam.getUserId())
                    .cartId(transactionSaveBodyParam.getCartId())
                    .total(transactionSaveBodyParam.getTotal())
                    .paymentMethod(transactionSaveBodyParam.getPaymentMethod())
                    .address(transactionSaveBodyParam.getAddress())
                    .status(Status.ACTIVE)
                    .build()
            );

        return new ResponseApi("OK", "Transaction saved", transaction.toTransactionDto());
    }

    public ResponseApi update(long id, TransactionUpdateBodyParam transactionUpdateBodyParam) {
        Transaction transaction = transactionRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Item not found"));

        transaction.setUserId(transactionUpdateBodyParam.getUserId());
        transaction.setCartId(transactionUpdateBodyParam.getCartId());
        transaction.setTotal(transactionUpdateBodyParam.getTotal());
        transaction.setStatus(transactionUpdateBodyParam.getStatus());
        transaction.setPaymentMethod(transactionUpdateBodyParam.getPaymentMethod());
        transaction.setAddress(transactionUpdateBodyParam.getAddress());

        TransactionDto transactionDto = transactionRepository.save(transaction).toTransactionDto();

        return new ResponseApi("OK", "Transaction updated", transactionDto);
    }

    public ResponseApi getTransaction(long transactionId) {
        TransactionDto transactionDto = transactionRepository.findById(transactionId)
            .orElseThrow(() -> new ResourceNotFoundException("Transaction Not Found"))
            .toTransactionDto();

        return new ResponseApi("OK", "Get transaction success", transactionDto);
    }

    public ResponseApi getListTransaction(long userId) {
        List<Transaction> transactions = transactionRepository.findByUserId(userId);
        List<TransactionDto> transactionDtos = transactions.stream().map(t ->
            new TransactionDto(
                t.getId(),
                t.getUserId(),
                t.getCartId(),
                t.getTotal(),
                t.getStatus(),
                t.getPaymentMethod(),
                t.getAddress(),
                t.getCreatedDate(),
                t.getUpdatedDate()
            )
        ).collect(Collectors.toList());

        return new ResponseApi("OK", "Get list transaction success", transactionDtos);
    }

    public ResponseApi delete(long id) {
        transactionRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Transaction Not Found"));
        transactionRepository.deleteById(id);

        return new ResponseApi("OK","Transaction deleted", null);
    }
}
