package com.bvk.transaction.controller;

import com.bvk.apienv.model.ResponseApi;
import com.bvk.transaction.model.db.param.TransactionSaveBodyParam;
import com.bvk.transaction.model.db.param.TransactionUpdateBodyParam;
import com.bvk.transaction.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/transaction")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @PostMapping
    public ResponseApi save(@Valid @RequestBody TransactionSaveBodyParam transactionSaveBodyParam) {
        return transactionService.save(transactionSaveBodyParam);
    }

    @PutMapping("/{id}")
    public ResponseApi update(@PathVariable long id,
                              @Valid @RequestBody TransactionUpdateBodyParam transactionUpdateBodyParam) {
        return transactionService.update(id, transactionUpdateBodyParam);
    }

    @GetMapping("/{id}")
    public ResponseApi getTransaction(@PathVariable long id) {
        return transactionService.getTransaction(id);
    }

    @GetMapping
    public ResponseApi getListTransaction(@RequestParam long userId) {
        return transactionService.getListTransaction(userId);
    }

    @DeleteMapping("/{id}")
    public ResponseApi delete(@PathVariable long id) { return transactionService.delete(id); }
}
