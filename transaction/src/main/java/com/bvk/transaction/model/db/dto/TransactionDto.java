package com.bvk.transaction.model.db.dto;

import com.bvk.transaction.model.db.value.PaymentMethod;
import com.bvk.transaction.model.db.value.Status;

import java.time.LocalDate;

public class TransactionDto {

    private long id;
    private long userId;
    private long cartId;
    private double total;
    private Status status;
    private PaymentMethod paymentMethod;
    private String address;
    private LocalDate createdDate;
    private LocalDate updatedDate;

    public TransactionDto() {}

    public TransactionDto(long id, long userId, long cartId, double total,
                          Status status, PaymentMethod paymentMethod,
                          String address, LocalDate createdDate, LocalDate updatedDate) {
        this.id = id;
        this.userId = userId;
        this.cartId = cartId;
        this.total = total;
        this.status = status;
        this.paymentMethod = paymentMethod;
        this.address = address;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getCartId() {
        return cartId;
    }

    public void setCartId(long cartId) {
        this.cartId = cartId;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDate getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
    }
}
