package com.bvk.transaction.model.db;

import com.bvk.transaction.model.db.dto.TransactionDto;
import com.bvk.transaction.model.db.value.PaymentMethod;
import com.bvk.transaction.model.db.value.Status;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "transaction")
public class Transaction {

    @Id
    @GeneratedValue
    @Column(name = "transaction_id")
    private long id;

    @NotNull(message = "User ID could not be empty.")
    private long userId;

    @NotNull(message = "Cart ID could not be empty.")
    private long cartId;

    @NotNull(message = "Total could not be empty.")
    private double total;

    @NotNull(message = "Status could not be empty.")
    @Enumerated(EnumType.STRING)
    private Status status;

    @NotNull(message = "Payment method could not be empty.")
    @Enumerated(EnumType.STRING)
    private PaymentMethod paymentMethod;

    @NotBlank(message = "Address could not be empty.")
    private String address;

    private String createdBy;

    @CreatedDate
    private LocalDate createdDate;

    private String updatedBy;

    @LastModifiedDate
    private LocalDate updatedDate;

    public TransactionDto toTransactionDto() {
        return new TransactionDto(
            this.id,
            this.userId,
            this.cartId,
            this.total,
            this.status,
            this.paymentMethod,
            this.address,
            this.createdDate,
            this.updatedDate
        );
    }
}
