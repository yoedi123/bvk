package com.bvk.transaction.model.db.param;

import com.bvk.transaction.model.db.value.PaymentMethod;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class TransactionSaveBodyParam {

    @NotNull(message = "User ID could not be empty.")
    private long userId;

    @NotNull(message = "Cart ID could not be empty.")
    private long cartId;

    private double total;

    @NotNull(message = "Payment method could not be empty.")
    @Enumerated(EnumType.STRING)
    private PaymentMethod paymentMethod;

    @NotBlank(message = "Address could not be empty.")
    private String address;

    public TransactionSaveBodyParam() {}

    public TransactionSaveBodyParam(long userId, long cartId,
                                    double total, PaymentMethod paymentMethod,
                                    String address) {
        this.userId = userId;
        this.cartId = cartId;
        this.total = total;
        this.paymentMethod = paymentMethod;
        this.address = address;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getCartId() {
        return cartId;
    }

    public void setCartId(long cartId) {
        this.cartId = cartId;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
