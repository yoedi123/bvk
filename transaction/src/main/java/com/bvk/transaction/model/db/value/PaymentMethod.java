package com.bvk.transaction.model.db.value;

public enum PaymentMethod {
    BANK_TRANSFER, INDOMARET, ALFAMART, COD
}
