package com.bvk.transaction.model.db.value;

public enum Status {
    ACTIVE, PAID, CANCELED
}
