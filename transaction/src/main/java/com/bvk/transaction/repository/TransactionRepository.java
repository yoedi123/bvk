package com.bvk.transaction.repository;

import com.bvk.transaction.model.db.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    List<Transaction> findByUserId(long userId);
}
