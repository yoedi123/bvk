package com.bvk.apienv.exception;

import com.bvk.apienv.model.ResponseApi;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> serverErrorException(
        Exception exception,
        WebRequest request
    ) {
//        ApiError apiError = new ApiError(LocalDateTime.now(), exception.getMessage(), request.getDescription(false));
        ResponseApi apiError = new ResponseApi("NOK", "Internal Server Error", null);
        return new ResponseEntity(apiError, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Object> resourceNotFoundException(
        ResourceNotFoundException exception,
        WebRequest request
    ) {
//        ApiError apiError = new ApiError(LocalDateTime.now(), exception.getMessage(), request.getDescription(false));
        ResponseApi apiError = new ResponseApi("NOK", exception.getMessage(), null);
        return new ResponseEntity(apiError, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<Object> validationException(
        ValidationException exception,
        WebRequest request
    ) {
//        ApiError apiError = new ApiError(LocalDateTime.now(), exception.getMessage(), request.getDescription(false));
        ResponseApi apiError = new ResponseApi("NOK", exception.getMessage(), null);
        return new ResponseEntity(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> methodArgumentNotValidException(
        MethodArgumentNotValidException exception,
        WebRequest request
    ) {
        ObjectError objectError = exception.getBindingResult().getAllErrors().get(0);
//        ApiError apiError = new ApiError(LocalDateTime.now(),
//                objectError.getDefaultMessage(),
//                request.getDescription(false)
//        );
        ResponseApi apiError = new ResponseApi("NOK", objectError.getDefaultMessage(), null);

        return new ResponseEntity(apiError, HttpStatus.BAD_REQUEST);
    }
}
