package com.bvk.item.service.hitapi;

import com.bvk.apienv.model.ResponseApi;
import com.bvk.item.model.param.TransactionSaveBodyParam;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface HitCheckout {

    @POST("/transaction")
    Call<ResponseApi> checkout(@Body TransactionSaveBodyParam transactionSaveBodyParam);
}
