package com.bvk.item.service;

import com.bvk.apienv.exception.ResourceNotFoundException;
import com.bvk.apienv.model.ResponseApi;
import com.bvk.item.model.db.Item;
import com.bvk.item.model.db.dto.ItemDto;
import com.bvk.item.model.param.ItemSaveBodyParam;
import com.bvk.item.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ItemService {

    @Autowired
    private ItemRepository itemRepository;

    public ResponseApi save(ItemSaveBodyParam itemParam) {
        Item item = Item.builder()
            .name(itemParam.getName())
            .category(itemParam.getCategory())
            .price(itemParam.getPrice())
            .stock(itemParam.getStock())
            .build();

        ItemDto itemDto = itemRepository.save(item).toItemDto();
        return new ResponseApi("OK","Item saved", itemDto);
    }

    public ResponseApi update(long id, ItemSaveBodyParam itemParam) {
        Item item = itemRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Item not found"));

        item.setName(itemParam.getName());
        item.setCategory(itemParam.getCategory());
        item.setPrice(itemParam.getPrice());
        item.setStock(itemParam.getStock());

        ItemDto itemDto = itemRepository.save(item).toItemDto();
        return new ResponseApi("OK","Item updated", itemDto);
    }

    public ResponseApi getItemById(long id) {
        ItemDto itemDto = itemRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Item Not Found"))
            .toItemDto();

        return new ResponseApi("OK","Get detail item success", itemDto);
    }

    public ResponseApi getAll() {
        List<Item> items = itemRepository.findAll();
        List<ItemDto> itemDto = items.stream().map( i ->
            ItemDto.builder()
                .id(i.getId())
                .name(i.getName())
                .category(i.getCategory())
                .price(i.getPrice())
                .stock(i.getStock())
                .createdBy(i.getCreatedBy())
                .createdDate(i.getCreatedDate())
                .updatedBy(i.getUpdatedBy())
                .updatedDate(i.getUpdatedDate())
                .build()
        ).collect(Collectors.toList());

        return new ResponseApi("OK", "Get all item success", itemDto);
    }

    public ResponseApi getItemByNameOrCategory(String name, String category) {
        List<Item> items = itemRepository.findByNameOrCategory(name, category);
        List<ItemDto> itemDto = items.stream().map( i ->
            ItemDto.builder()
                .id(i.getId())
                .name(i.getName())
                .category(i.getCategory())
                .price(i.getPrice())
                .stock(i.getStock())
                .createdBy(i.getCreatedBy())
                .createdDate(i.getCreatedDate())
                .updatedBy(i.getUpdatedBy())
                .updatedDate(i.getUpdatedDate())
                .build()
        ).collect(Collectors.toList());

        return new ResponseApi("OK","Get list item success", itemDto);
    }

    public ResponseApi delete(long id) {
        itemRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Item not found"));
        itemRepository.deleteById(id);

        return new ResponseApi("OK","Item deleted", null);
    }
}
