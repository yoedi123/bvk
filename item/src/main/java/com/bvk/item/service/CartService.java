package com.bvk.item.service;

import com.bvk.apienv.exception.ResourceNotFoundException;
import com.bvk.apienv.exception.ValidationException;
import com.bvk.apienv.model.ResponseApi;
import com.bvk.item.model.db.Cart;
import com.bvk.item.model.db.CartItem;
import com.bvk.item.model.db.Item;
import com.bvk.item.model.db.value.Status;
import com.bvk.item.model.param.CartSaveBodyParam;
import com.bvk.item.model.param.CheckoutParam;
import com.bvk.item.model.param.TransactionSaveBodyParam;
import com.bvk.item.repository.CartItemRepository;
import com.bvk.item.repository.CartRepository;
import com.bvk.item.repository.ItemRepository;
import com.bvk.item.service.hitapi.HitCheckout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.io.IOException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class CartService {

    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private CartItemRepository cartItemRepository;
    @Autowired
    private ItemRepository itemRepository;

    public ResponseApi addToCart(CartSaveBodyParam cartParam) {
        Cart activeCart = cartRepository.findByUserIdAndStatus(
            cartParam.getUserId(), Status.ACTIVE);
        Item item = itemRepository.findById(cartParam.getItemId())
            .orElseThrow(() -> new ResourceNotFoundException("Item Not Found"));

        int stockCurrent = item.getStock() - cartParam.getQuantity();
        if (item.getStock() <= 0 || stockCurrent < 0) {
            throw new ValidationException("Out of stock");
        }

        activeCart = Optional.ofNullable(activeCart)
            .orElseGet(() ->
                cartRepository.save(
                    Cart.builder()
                        .userId(cartParam.getUserId())
                        .status(Status.ACTIVE)
                        .build()
                )
            );

        CartItem existingCartItem = cartItemRepository.findByCart_UserIdAndItem_Id(
                activeCart.getUserId(), item.getId());

        Long cartItemId = Optional.ofNullable(existingCartItem)
            .filter(ci -> ci.getItem().getId() == item.getId())
            .map(CartItem::getId)
            .orElse(null);

        int cartItemQuantity = Optional.ofNullable(existingCartItem)
            .filter(ci -> ci.getItem().getId() == item.getId())
            .map(CartItem::getQuantity)
            .orElse(0);

        cartParam.setQuantity(cartItemQuantity + cartParam.getQuantity());

        CartItem cartItem = CartItem.builder()
            .cart(activeCart)
            .item(item)
            .quantity(cartParam.getQuantity())
            .build();

        if (cartItemId != null) cartItem.setId(cartItemId);

        item.setStock(stockCurrent);
        itemRepository.save(item);

        cartItem = cartItemRepository.saveAndFlush(cartItem);
        activeCart.setCartItemList(activeCart.getCartItemList() == null
            ? new HashSet() : activeCart.getCartItemList());
        activeCart.getCartItemList().add(cartItem);

        return new ResponseApi("OK", "Add to cart success", activeCart.toCartList());
    }

    public ResponseApi removeItem(long id, long itemId) {
        Cart activeCart = cartRepository.findByIdAndStatus(id, Status.ACTIVE)
            .orElseThrow(() -> new ResourceNotFoundException("Cart not found"));
        Item item = itemRepository.findById(itemId)
            .orElseThrow(() -> new ResourceNotFoundException("Item not found"));

        CartItem deletedCartItem = activeCart.getCartItemList().stream()
            .filter(ci -> ci.getItem().getId() == item.getId()).findFirst()
            .orElseThrow(() -> new ResourceNotFoundException("Item in cart not found"));

        activeCart.getCartItemList().remove(deletedCartItem);
        cartItemRepository.deleteById(deletedCartItem.getId());

        return new ResponseApi("OK", "Remove item from cart success", activeCart.toCartList());
    }

    public ResponseApi checkout(long id, CheckoutParam checkoutParam) {

        Cart cart = cartRepository.findByIdAndStatus(id, Status.ACTIVE)
            .orElseThrow(() -> new ResourceNotFoundException("Cart not found"));
        Set<CartItem> cartItems = Optional.ofNullable(cart.getCartItemList())
            .orElseThrow(() -> new ValidationException("No item in this cart"));

        double total = cartItems.stream().mapToDouble(
            ci -> ci.getItem().getPrice() * ci.getQuantity()).sum();

        if(total == 0) {
            throw new ValidationException("No item in this cart");
        }

        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://localhost:8082")
            .addConverterFactory(JacksonConverterFactory.create())
            .build();

        HitCheckout hitCheckout = retrofit.create(HitCheckout.class);
        TransactionSaveBodyParam transactionSaveBodyParam = TransactionSaveBodyParam.builder()
            .userId(cart.getUserId())
            .cartId(cart.getId())
            .paymentMethod(checkoutParam.getPaymentMethod())
            .address(checkoutParam.getAddress())
            .total(total)
            .build();

        ResponseApi responeCheckout = null;
        String statusResp = "NOK";
        String messageResp = "Checkout failed";
        try {
            Call<ResponseApi> callCheckout = hitCheckout.checkout(transactionSaveBodyParam);
            responeCheckout = callCheckout.execute().body();

            if (Optional.ofNullable(responeCheckout)
                    .map(it -> it.getStatus())
                    .orElse("NOK").equals("OK")) {
                statusResp = "OK";
                messageResp = "Checkout success";
            }
        } catch (IOException e) {
            throw new ValidationException(e.getMessage());
        }

        return new ResponseApi(statusResp, messageResp,null);
    }
}
