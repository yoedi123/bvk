package com.bvk.item.controller;

import com.bvk.apienv.model.ResponseApi;
import com.bvk.item.model.param.CartSaveBodyParam;
import com.bvk.item.model.param.CheckoutParam;
import com.bvk.item.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/cart")
public class CartController {

    @Autowired
    private CartService cartService;

    @PostMapping
    public ResponseApi addToCart(@Valid @RequestBody CartSaveBodyParam cartSaveBodyParam) {
        return cartService.addToCart(cartSaveBodyParam);
    }

    @PatchMapping("/{id}")
    public ResponseApi removeItem(@NotNull @PathVariable long id,
                                  @NotNull @RequestParam long itemId) {
        return cartService.removeItem(id, itemId);
    }

    @PatchMapping("/{id}/checkout")
    public ResponseApi checkout(@NotNull @PathVariable long id,
                                @Valid @RequestBody CheckoutParam checkoutParam) {
        return cartService.checkout(id, checkoutParam);
    }
}
