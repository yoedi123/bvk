package com.bvk.item.controller;

import com.bvk.apienv.model.ResponseApi;
import com.bvk.item.model.param.ItemSaveBodyParam;
import com.bvk.item.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/item")
public class ItemController {

    @Autowired
    private ItemService itemService;

    @PostMapping
    public ResponseApi save(@Valid @RequestBody ItemSaveBodyParam itemParam) {
        return itemService.save(itemParam);
    }

    @PutMapping("/{id}")
    public ResponseApi update(@PathVariable long id,
                              @Valid @RequestBody ItemSaveBodyParam itemParam) {
        return itemService.update(id, itemParam);
    }

    @GetMapping("/all")
    public ResponseApi getAll() {
        return itemService.getAll();
    }

    @GetMapping("/{id}")
    public ResponseApi getItem(@PathVariable long id) {
        return itemService.getItemById(id);
    }

    @GetMapping
    public ResponseApi getItemByNameOrCategory(
        @RequestParam String name,
        @RequestParam String category
    ) {
        return itemService.getItemByNameOrCategory(name, category);
    }

    @DeleteMapping("/{id}")
    public ResponseApi delete(@PathVariable long id) {
        return itemService.delete(id);
    }
}
