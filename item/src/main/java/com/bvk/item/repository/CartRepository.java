package com.bvk.item.repository;

import com.bvk.item.model.db.Cart;
import com.bvk.item.model.db.value.Status;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CartRepository extends JpaRepository<Cart, Long> {

    Cart findByUserIdAndStatus(long userId, Status status);
    Optional<Cart> findByIdAndStatus(long id, Status status);
}
