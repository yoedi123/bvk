package com.bvk.item.repository;

import com.bvk.item.model.db.Item;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ItemRepository extends JpaRepository<Item, Long> {

    List<Item> findByNameOrCategory(String name, String category);
}
