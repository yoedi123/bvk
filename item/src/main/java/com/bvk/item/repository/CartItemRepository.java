package com.bvk.item.repository;

import com.bvk.item.model.db.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartItemRepository extends JpaRepository<CartItem, Long> {

    CartItem findByCart_UserIdAndItem_Id(long userId, long id);
}
