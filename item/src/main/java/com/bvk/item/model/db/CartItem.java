package com.bvk.item.model.db;

import com.bvk.item.model.db.dto.CartItemList;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "cart_item")
public class CartItem {

    @Id
    @GeneratedValue
    @Column(name = "cart_item_id")
    private long id;

    @JsonBackReference("cart")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cart_id", referencedColumnName = "cart_id")
    private Cart cart;

    @JsonBackReference("item")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_id", referencedColumnName = "item_id")
    private Item item;

    private int quantity;

    private String createdBy;

    @CreatedDate
    private LocalDate createdDate;

    private String updatedBy;

    @LastModifiedDate
    private LocalDate updatedDate;

    public CartItemList toCartItemList() {
        return CartItemList.builder()
            .cartItemId(this.id)
            .itemId(this.item.getId())
            .quantity(this.quantity)
            .build();
    }
}
