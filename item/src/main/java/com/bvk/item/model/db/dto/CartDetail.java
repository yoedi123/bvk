package com.bvk.item.model.db.dto;

import com.bvk.item.model.db.value.Status;
import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CartDetail {

    private long cartId;
    private long userId;
    private Status status;
    private Set<CartItemList> cartItems;
}
