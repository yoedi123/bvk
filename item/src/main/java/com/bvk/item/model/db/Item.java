package com.bvk.item.model.db;

import com.bvk.item.model.db.dto.ItemDto;
import com.bvk.item.model.db.value.Category;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "item")
public class Item {

    @Id
    @GeneratedValue
    @Column(name = "item_id")
    private long id;

    @NotBlank(message = "Name could not be empty.")
    @Pattern(regexp = "^[\\w\\s]*$", message = "Name must only word character")
    private String name;

    @NotNull(message = "Category could not be empty.")
    @Enumerated(EnumType.STRING)
    private Category category;

    private double price;

    private int stock;

    @JsonManagedReference("item")
    @OneToMany(mappedBy = "item", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<CartItem> cartItemList;

    private String createdBy;

    @CreatedDate
    private LocalDate createdDate;

    private String updatedBy;

    @LastModifiedDate
    private LocalDate updatedDate;

    public ItemDto toItemDto() {
        return ItemDto.builder()
            .id(this.id)
            .name(this.name)
            .category(this.category)
            .price(this.price)
            .stock(this.stock)
            .createdBy(this.createdBy)
            .createdDate(this.createdDate)
            .updatedBy(this.updatedBy)
            .updatedDate(this.updatedDate)
            .build();
    }
}
