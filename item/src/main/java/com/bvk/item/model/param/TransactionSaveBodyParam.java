package com.bvk.item.model.param;

import com.bvk.item.model.db.value.PaymentMethod;
import lombok.*;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TransactionSaveBodyParam {

    @NotNull(message = "User ID could not be empty.")
    private long userId;

    @NotNull(message = "Cart ID could not be empty.")
    private long cartId;

    private double total;

    @NotNull(message = "Payment method could not be empty.")
    @Enumerated(EnumType.STRING)
    private PaymentMethod paymentMethod;

    @NotBlank(message = "Address could not be empty.")
    private String address;
}
