package com.bvk.item.model.db.value;

public enum PaymentMethod {
    BANK_TRANSFER, INDOMARET, ALFAMART, COD
}
