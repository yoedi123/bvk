package com.bvk.item.model.db.dto;

import com.bvk.item.model.db.value.Category;
import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ItemDto {

    private long id;
    private String name;
    private Category category;
    private double price;
    private int stock;
    private String createdBy;
    private LocalDate createdDate;
    private String updatedBy;
    private LocalDate updatedDate;
}
