package com.bvk.item.model.param;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CartSaveBodyParam {

    private long userId;
    private long itemId;
    private int quantity;
}
