package com.bvk.item.model.param;

import com.bvk.item.model.db.value.Category;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@NoArgsConstructor
public class ItemSaveBodyParam {

    @NotBlank(message = "Name could not be empty.")
    @Pattern(regexp = "^[\\w\\s]*$", message = "Name must only word character")
    private String name;

    @NotNull(message = "Category could not be empty.")
    @Enumerated(EnumType.STRING)
    private Category category;

    private double price;

    private int stock;
}
