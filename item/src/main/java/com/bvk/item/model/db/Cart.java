package com.bvk.item.model.db;

import com.bvk.item.model.db.dto.CartDetail;
import com.bvk.item.model.db.dto.CartItemList;
import com.bvk.item.model.db.value.Status;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "cart")
public class Cart {

    @Id
    @GeneratedValue
    @Column(name = "cart_id")
    private long id;

    @NotNull(message = "User ID could not be empty.")
    private long userId;

    @NotNull(message = "Status could not be empty.")
    @Enumerated(EnumType.STRING)
    private Status status;

    @JsonManagedReference("cart")
    @OneToMany(mappedBy = "cart", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<CartItem> cartItemList;

    private String createdBy;

    @CreatedDate
    private LocalDate createdDate;

    private String updatedBy;

    @LastModifiedDate
    private LocalDate updatedDate;

    public String toString() {
        return "";
    }

    public CartDetail toCartList() {
        Set<CartItemList> cartItems = this.cartItemList.stream()
                .map(ci -> ci.toCartItemList()).collect(Collectors.toSet());

        return CartDetail.builder()
            .cartId(this.id)
            .userId(this.userId)
            .status(this.status)
            .cartItems(cartItems)
            .build();
    }
}
