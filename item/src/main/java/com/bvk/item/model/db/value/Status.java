package com.bvk.item.model.db.value;

public enum Status {
    ACTIVE, CHECKOUT, CANCELED
}
