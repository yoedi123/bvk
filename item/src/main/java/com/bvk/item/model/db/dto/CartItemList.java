package com.bvk.item.model.db.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Builder
public class CartItemList {

    private long cartItemId;
    private long itemId;
    private int quantity;

    public CartItemList(long cartItemId, long itemId, int quantity) {
        this.cartItemId = cartItemId;
        this.itemId = itemId;
        this.quantity = quantity;
    }
}
